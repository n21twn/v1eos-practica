#include "shell.hh"

int main()
{ std::string input;

  int fd_1 = syscall(SYS_open, "testprompt", O_RDONLY, 0755); 
  char byte[1]; 
  std::string prompt = "";                                          
  while(syscall(SYS_read, fd_1, byte, 1)) 
    prompt += byte;                     
  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
 

  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file() // ToDo: Implementeer volgens specificatie.
{ 
  std::string filename;
  std::string message;
  std::string new_message;

  std::cout<<"voer hier nieuwe bestands naam in:";
  std::cin>>filename;
  
  std::cout<<"voer hier bericht in:";
  while (std::getline(std::cin,message)){
    
    if (message == "<EOF>"){
      break;
    }
    new_message += message;
  }

  const char* filename_c = filename.c_str();
  const char* message_c[] = {new_message.c_str()};

  int created_file = syscall(SYS_creat,filename_c, 0755);
  syscall(SYS_write,created_file, message_c[0], new_message.size());
  
}

void list() // ToDo: Implementeer volgens specificatie.
{ 


  const char* args[] = {"/bin/ls", "-l","-a",NULL};

  if(syscall(SYS_fork) == 0){
    syscall(SYS_execve, args[0], args, NULL);
   
  }else{
    syscall(SYS_wait4, 0,NULL,NULL,NULL);
  }
 }

void find() // ToDo: Implementeer volgens specificatie.
{ 
  std::string test;
  std::cin>> test;

  const char* test2 = test.c_str();
  
  
  int fd_2[2];

  syscall(SYS_pipe, &fd_2);

  syscall(SYS_close, fd_2[0]);
  syscall(SYS_dup2, fd_2[1],1);

  int id_find = syscall(SYS_fork);
  if(id_find == 0){
    const char* args_find[] = {"/usr/bin/find",".",NULL};

    syscall(SYS_close, fd_2[0]);
    syscall(SYS_dup2, fd_2[1],1); 
    syscall(SYS_execve, args_find[0], args_find, NULL);
  }else{
    int id_seek = syscall(SYS_fork);

    if (id_seek == 0)
    {
      const char* args_grep[] = {"/bin/grep",test2,NULL};
      syscall(SYS_close, fd_2[1]);
      syscall(SYS_dup2, fd_2[0],0);
      syscall(SYS_execve, args_grep[0], args_grep, NULL);
    }else{
      syscall(SYS_close,fd_2[0]);
      syscall(SYS_close,fd_2[1]);

      syscall(SYS_wait4, id_seek,NULL,NULL,NULL);
      syscall(SYS_wait4, id_find,NULL,NULL,NULL);
    }
  }
  std::cout << "ToDo: Implementeer hier find()" << std::endl;
}

void seek() // ToDo: Implementeer volgens specificatie.
{

  std::string loop_message = "x";
  std::string file_name = "loop";
  
  for (unsigned int i = 0; i < 5000000; i++)
  {
    loop_message += '\0';
  }
  loop_message += "x";

  const char* filename_c = file_name.c_str();
  const char* message_c[] = {loop_message.c_str()};

  int created_file = syscall(SYS_creat,filename_c, 0755);
  syscall(SYS_write,created_file, message_c[0], loop_message.size());


  std::string seek_name = "seek";

  const char* seek_c = seek_name.c_str();

  int created_seek = syscall(SYS_creat,seek_c, 0755);
  syscall(SYS_write,created_seek, "x",1);

  int seek_message = syscall(SYS_lseek, created_seek, 5000001, SEEK_SET);
  syscall(SYS_write,created_seek, seek_message, 5000001);
  syscall(SYS_write,created_seek, "x", 1);
   }

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
